# README #

This project is about building a web service for controlling water supply across regions in Bandung. Data are collected
through dummy sensors real-time using Go. Users are able to access these data with a dedicated front-end or by making
REST requests to the server. Data is transmitted every 1 minute.

By Sergio Ryan/18215014

## FUNCTIONAL REQUIREMENTS ##
1. Sistem dapat mengumpulkan data air
    1.1 Sistem dapat mengumpulkan data air dari titik - titik suplai air
    1.2 Data air yang dikumpulkan berupa volume air yang disuplai, dan debit air tersedia
    1.3 Data dikumpulkan secara periodik setiap 1 menit
2. Sistem dapat menyimpan data air di database
    2.1 Sistem akan menyimpan debit air, dan volume air, di titik - titik suplai air
    2.2 Setiap 1 menit, database akan berubah dengan data yang baru karena data dikumpulkan tiap 1 menit
3. Sistem dapat menampilkan data dari database
    3.1 Sistem menerima request GET yang berisi lokasi titik suplai dan memberikan data air di titik suplai yang diminta
    3.2 Sistem menerima request GET yang berisi lokasi sembarang dan memberikan data suplai air titik suplai terdekat
    3.3 Sistem menerima request GET yang berisi lokasi sembarang, dan jumlah debit air, dan sistem dapat menentukan
        apakah debit air dengan jumlah tertentu tersebut boleh disediakan oleh titik suplai terdekat. Misalnya apabila
        seseorang akan membangun suatu pabrik di suatu lokasi X, dan membutuhkan air dari titik suplai terdekat sebesar
        Y debit air, maka sistem harus dapat menentukan apakah debit air sejumlah Y diperbolehkan.
    3.4 Untuk poin 3.3, sistem harus dapat mempertimbangkan persediaan air saat kemarau yang tentunya lebih sedikit.
4. Sistem dapat menambah data ke database
    4.1 Sistem menerima request PUT yang berisi lokasi titik suplai, dan data air di titik suplai tersebut, untuk
        ditambahkan ke database. Request ini pada dasarnya dilakukan setiap 1 menit.
    4.2 Sistem menerima request PUT tidak hanya dari 1 melainkan minimal 5 titik suplai dalam waktu yang sama setiap
        menitnya.
    4.3 Setiap request PUT akan menambahkan sebuah entri di database, bukan mengupdate yang lama. Sehingga sistem
        menyimpan histori mengenai data air di setiap titik setiap waktunya.
    4.4 Untuk melakukan hal ini, diperlukan subsistem lagi untuk melakukan request PUT yang secara simultan dijalankan
        di titik - titik suplai (dummy).
5. Sistem dapat secara periodik menghapus data dari database
    5.1 Ada subsistem yang berjalan yang berfungsi untuk menghapus data yang umurnya sudah lebih dari 1 minggu
    5.2 Sistem tidak menerima request DELETE untuk menghapus data secara on-demand untuk alasan keamanan.
6. Sistem dapat berjalan di OS Linux tanpa GUI

## NON-FUNCTIONAL REQUIREMENTS ##
1. Sistem mampu mengurus minimal 5 request sekaligus dalam 1 waktu, dan dilakukan setiap menit
2. Sistem memiliki database dan space yang cukup untuk menyimpan database selama 1 minggu
3. Sistem memiliki database yang mampu menerima input lebih dari 5 data tiap menit 24/7
4. Sistem menyediakan tampilan yang rapi bagi pengguna
5. Pengguna tidak boleh menunggu lebih dari 3 detik untuk membuka interface sistem dengan asumsi pengguna berada di
   satu subnet dengan sistem. Artinya apabila pengguna menunggu lebih dari 3 detik akibat kecepatan internet mereka,
   ini diperbolehkan
6. Sistem mampu berjalan di lingkungan dengan RAM tidak lebih dari 4 GB

## HARDWARE/SOFTWARE REQUIREMENTS ##
1. Server
    1.1 Prosesor minimal dual core dengan clock minimal 2 GHz
    1.2 RAM minimal 1 GB
    1.3 Space di disk minimal 10 GB
    1.4 OS Linux tanpa GUI (yang akan digunakan adalah Arch Linux)
2. Client
    1.1 OS dengan GUI (Windows, OS X, atau Linux dengan GUI)
    1.2 Browser, direkomendasikan yang modern seperti Chrome dan Firefox
    1.3 Mobile/desktop tidak menjadi masalah selama bisa menjalankan browser