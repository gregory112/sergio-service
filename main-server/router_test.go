package main

import (
	"net/http/httptest"
	"testing"
)

func TestSetRoute(t *testing.T) {
	tests := []struct {
		url  string
		want int
	}{
		{"/css/index.css", 200},
		{"/index.html", 301},
		{"/", 200},
	}
	SetRoute()

	for _, test := range tests {
		writer := httptest.NewRecorder()
		request := httptest.NewRequest("GET", test.url, nil)

		router.ServeHTTP(writer, request)

		if writer.Code != test.want {
			t.Errorf("Response code is %v when requested %s", writer.Code, test.url)
		}

		if writer.Body.String() == "404 page not found" {
			t.Errorf("Response body is %s", writer.Body.String())
		}
	}

}

func TestAttachSensor(t *testing.T) {
	AttachSensor(router)
	url := "/sensor/1"
	writer := httptest.NewRecorder()
	request := httptest.NewRequest("GET", url, nil)

	router.ServeHTTP(writer, request)

	if writer.Code != 200 {
		t.Errorf("Response code is %v when requested %s", writer.Code, url)
	}

	if writer.Body.String() == "" {
		t.Errorf("Response body is empty")
	}

	t.Logf("Response body: %s", writer.Body.String())
}

func TestAttachSensorExtra(t *testing.T) {
	AttachSensorExtra(router)
	url := "/sensor/1?all"
	writer := httptest.NewRecorder()
	request := httptest.NewRequest("GET", url, nil)

	router.ServeHTTP(writer, request)

	if writer.Code != 200 {
		t.Errorf("Response code is %v when requested %s", writer.Code, url)
	}

	if writer.Body.String() == "" {
		t.Errorf("Response body is empty")
	}

	t.Logf("Response body: %s", writer.Body.String())
}

func TestAttachSensorAll(t *testing.T) {
	AttachSensorAll(router)
	url := "/sensor"
	writer := httptest.NewRecorder()
	request := httptest.NewRequest("GET", url, nil)

	router.ServeHTTP(writer, request)

	if writer.Code != 200 {
		t.Errorf("Response code is %v when requested %s", writer.Code, url)
	}

	if writer.Body.String() == "" {
		t.Errorf("Response body is empty")
	}

	t.Logf("Response body: %s", writer.Body.String())
}

func TestAttachUsers(t *testing.T) {
	AttachUsers(router)
	url := "/sensor/1?users"
	writer := httptest.NewRecorder()
	request := httptest.NewRequest("GET", url, nil)

	router.ServeHTTP(writer, request)

	if writer.Code != 200 {
		t.Errorf("Response code is %v when requested %s", writer.Code, url)
	}

	if writer.Body.String() == "" {
		t.Errorf("Response body is empty")
	}

	t.Logf("Response body: %s", writer.Body.String())
}

func TestAttachUsersAll(t *testing.T) {
	AttachUsersAll(router)
	url := "/heavy"
	writer := httptest.NewRecorder()
	request := httptest.NewRequest("GET", url, nil)

	router.ServeHTTP(writer, request)

	if writer.Code != 200 {
		t.Errorf("Response code is %v when requested %s", writer.Code, url)
	}

	if writer.Body.String() == "" {
		t.Errorf("Response body is empty")
	}

	t.Logf("Response body: %s", writer.Body.String())
}

func TestAttachAnalytic(t *testing.T) {
	AttachAnalytic(router)
	url := "/sensor/1?analytic"
	writer := httptest.NewRecorder()
	request := httptest.NewRequest("GET", url, nil)

	router.ServeHTTP(writer, request)

	if writer.Code != 200 {
		t.Errorf("Response code is %v when requested %s", writer.Code, url)
	}

	if writer.Body.String() == "" {
		t.Errorf("Response body is empty")
	}

	t.Logf("Response body: %s", writer.Body.String())
}
