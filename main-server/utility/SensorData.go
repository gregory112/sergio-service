// utility package provides utility functions and data structures.
package utility

type SensorData struct {
	Id     int     `json:"-"`
	Volume float32 `json:"volume"`
	Flow   float32 `json:"flow"`
	Time   int64   `json:"time"`
}
