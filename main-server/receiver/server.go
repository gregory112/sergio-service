// Package receiver provides functionality for the router to listen to supplier connections.
// This package adds handler to the router to listen to /upload. Connections will be made from all suppliers to this
// URL.
//
// This package also provides a function to enable timeout function. See AttachTimeout and DetachTimeout.
package receiver

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"

	"sergio-service/main-server/database"
	"sergio-service/main-server/utility"

	"github.com/gorilla/mux"
)

var mu sync.Mutex
var timers map[int]*time.Timer
var stop chan struct{}
var suppliers chan timerStruct

type timerStruct struct {
	id    int
	timer *time.Timer
}

// AttachAddData attaches data insertion logic to router.
// Call AttachAddData before calling the ServeHTTP method. AttachAddData will attach the function to listen to sensor
// data. This function also creates new timer for a specific supplier. This timer is then used by AttachTimeout to
// update the database setting the supplier to not active. The logic for this is if a timer does not send any data in 45
// seconds after the last send, then that supplier data is going to expire and the supplier is set to inactive if
// AttachTimeout is called. Every time a supplier upload a data, its timer is then reset and the 45 seconds timeout is
// started again. If a supplier continuously sends its data in 30 seconds interval, then its data are not going to be
// deleted by the timeout.
func AttachAddData(router *mux.Router) {
	timers = make(map[int]*time.Timer)
	suppliers = make(chan timerStruct, 20)
	router.HandleFunc("/upload/{id}", func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		id, err := strconv.Atoi(vars["id"])
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}

		mu.Lock()
		if timers[id] != nil {
			timers[id].Reset(45 * time.Second)
		} else {
			timers[id] = time.NewTimer(45 * time.Second)
			suppliers <- timerStruct{id: id, timer: timers[id]}
		}
		mu.Unlock()

		data := utility.SensorData{}
		data.Id = id
		body, err := ioutil.ReadAll(request.Body)
		log.Printf("Request received to %s", request.URL.Path)
		defer request.Body.Close()

		if err != nil {
			log.Println(err)
			writer.WriteHeader(403)
			return
		}

		err = json.Unmarshal(body, &data)
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}
		data.Id = id

		err = database.UpdateSupplier(data)
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}
		err = database.UpdateAnalytic(data)
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}
	})
}

// AttachTimeout continuously poll the timer of each supplier and waits until the timer runs out, then call StopSupplier
// This method will change the supplier state to inactive after it has not sent its data for 45 seconds.
func AttachTimeout() {
	stop = make(chan struct{})
	go func() {
		for t := range suppliers {
			go func(entry timerStruct) {
				log.Printf("Timeout timer for supplier %d dispatched", entry.id)
				for {
					select {
					case <-entry.timer.C:
						database.StopSupplier(entry.id)
						log.Printf("Supplier %d timeout, updated", entry.id)
					case <-stop:
						mu.Lock()
						entry.timer.Stop()
						mu.Unlock()
						return
					}
				}
			}(t)
		}
	}()
}

// DetachTimeout kills the timeout goroutine that was fired by AttachTimeout
func DetachTimeout() {
	close(stop)
}
