package receiver

import (
	"bytes"
	"net/http/httptest"
	"testing"
	"time"

	"sergio-service/main-server/database"
	"sergio-service/main-server/utility"

	"github.com/gorilla/mux"
)

func TestAttachAddData(t *testing.T) {
	router := mux.NewRouter()
	AttachAddData(router)

	writer := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/upload/1", bytes.NewBufferString("{\"volume\":1.0,\"flow\":1.3}"))

	router.ServeHTTP(writer, request)

	if writer.Code != 200 {
		t.Errorf("Response code is %v", writer.Code)
	}

	if writer.Body.String() != "" {
		t.Errorf("Response body is %s", writer.Body.String())
	}
}

func TestAttachTimeout(t *testing.T) {
	data := utility.SensorData{
		Id:     1,
		Volume: 1.3,
		Flow:   1.3,
		Time:   121324123,
	}

	suppliers = make(chan timerStruct, 20)

	timers[data.Id] = time.NewTimer(5 * time.Second)
	suppliers <- timerStruct{data.Id, timers[data.Id]}

	database.UpdateSupplier(data)
	supplier := database.GetSupplier(data.Id)

	if supplier.SupplierID != 1 || supplier.Active != true {
		t.Fatal("Failed to update supplier activation status.")
	}

	AttachTimeout()
	defer DetachTimeout()

	time.Sleep(6 * time.Second)

	supplier = database.GetSupplier(data.Id)

	if supplier.SupplierID != 1 || supplier.Active == true {
		t.Fatal("Supplier is still active in database.")
	}
}

func TestDetachTimeout(t *testing.T) {
	data := utility.SensorData{
		Id:     1,
		Volume: 1.3,
		Flow:   1.3,
		Time:   121324123,
	}

	timers[data.Id] = time.NewTimer(5 * time.Second)
	suppliers <- timerStruct{data.Id, timers[data.Id]}
	database.UpdateSupplier(data)
	supplier := database.GetSupplier(data.Id)

	if supplier.SupplierID != 1 || supplier.Active != true {
		t.Fatal("Failed to update supplier activation status.")
	}

	AttachTimeout()
	DetachTimeout()

	time.Sleep(6 * time.Second)

	supplier = database.GetSupplier(data.Id)

	if supplier.SupplierID != 1 || supplier.Active != true {
		t.Fatal("Supplier is killed by timeout in database.")
	}
}
