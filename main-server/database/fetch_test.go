package database

import (
	"testing"
)

func TestGetSupplier(t *testing.T) {
	cases := []struct {
		id   int
		want string
	}{
		// Dependency: MySQL database with table named suppliers and an entry with ID 1 with supplier_name "Pasopati"
		{id: 1, want: "Pasopati"},
		{id: 100, want: ""},
		{id: -1, want: ""},
	}

	for _, test := range cases {
		supplier := GetSupplier(test.id)
		if test.want == "" {
			if supplier != nil {
				t.Errorf("Data mismatched, want nil, got a supplier")
			}
			continue
		}
		if supplier.SupplierName != test.want {
			t.Errorf("Name mismatched, want %s, got %s", test.want, supplier.SupplierName)
		}
	}
}

func TestGetSupplierAll(t *testing.T) {
	suppliers := GetSupplierAll()
	if len(suppliers) <= 1 {
		t.Error("Only zero or one suppliers fetched. Want more than 1")
	}
}

func TestGetSupplierAllWithAnalytics(t *testing.T) {
	suppliers := GetSupplierAllWithAnalytics()
	if len(suppliers[0].Analytics) <= 1 {
		t.Error("Only zero or one analytic entry fetched. Want more than 1")
	}
}

func TestGetSupplierAllWithHeavyUsers(t *testing.T) {
	suppliers := GetSupplierAllWithHeavyUsers()
	if len(suppliers[0].HeavyUsers) <= 1 {
		t.Error("Only zero or one heavy users entry fetched. Want more than 1")
	}
}

func TestGetSupplierWithAll(t *testing.T) {
	cases := []struct {
		id   int
		want int
	}{
		// Dependency: MySQL database with table named suppliers and an entry with ID 1 with an entry in analytics and
		// heavy users table
		{id: 1, want: 1},
		{id: 100, want: 0},
		{id: -1, want: 0},
	}

	for _, test := range cases {
		supplier := GetSupplierWithAll(test.id)
		if test.want == 0 {
			//if len(supplier.Analytics) != 0 || len(supplier.HeavyUsers) != 0 {
			if supplier != nil {
				t.Fatalf("Can still get analytics and heavy users data when invalid supplier is queried."+
					" ID queried: %d",
					test.id)
			}
			return
		}
		if supplier.Analytics[1].SupplierID != test.want {
			t.Errorf("ID (in Analytics field) mismatched, want %d, got %d", test.want, supplier.SupplierID)
		}
		if supplier.HeavyUsers[0].SupplierID != test.want {
			t.Errorf("ID (in HeavyUsers field) mismatched, want %d, got %d", test.want, supplier.SupplierID)
		}
	}
}

func TestGetSupplierWithAnalytics(t *testing.T) {
	cases := []struct {
		id   int
		want int
	}{
		// Dependency: MySQL database with table named suppliers and an entry with ID 1 with an entry in analytics table
		{id: 1, want: 1},
		{id: 100, want: 0},
		{id: -1, want: 0},
	}

	for _, test := range cases {
		supplier := GetSupplierWithAnalytics(test.id)
		if test.want == 0 {
			//if len(supplier.Analytics) != 0 {
			if supplier != nil {
				t.Fatalf("Can still get analytics data when invalid supplier is queried. ID queried: %d",
					test.id)
			}
			return
		}
		if supplier.Analytics[1].SupplierID != test.want {
			t.Errorf("ID mismatched, want %d, got %d", test.want, supplier.SupplierID)
		}
	}
}

func TestGetSupplierWithHeavyUsers(t *testing.T) {
	cases := []struct {
		id   int
		want int
	}{
		// Dependency: MySQL database with table named suppliers and an entry with ID 1 with an entry in heavy users
		// table
		{id: 1, want: 1},
		{id: 100, want: 0},
		{id: -1, want: 0},
	}

	for _, test := range cases {
		supplier := GetSupplierWithHeavyUsers(test.id)
		if test.want == 0 {
			//if len(supplier.HeavyUsers) != 0 {
			if supplier != nil {
				t.Fatalf("Can still get heavy users data when invalid supplier is queried. ID queried: %d",
					test.id)
			}
			return
		}
		if supplier.HeavyUsers[0].SupplierID != test.want {
			t.Errorf("ID mismatched, want %d, got %d", test.want, supplier.SupplierID)
		}
	}
}

// TestGetClosestSupplier requires a supplier entry in the database that has lat and lng +- 20 from the test case.
func TestGetClosestSupplier(t *testing.T) {
	lat := float32(-6.12)
	lng := float32(107.221)
	maxFlow := float32(10.212)

	suppliers := GetClosestSupplier(lat, lng, maxFlow)
	if suppliers == nil {
		t.Fatal("No suppliers are returned. Probably the database is not set up correctly?")
	}

	if suppliers[0].SupplierID == 0 {
		t.Errorf("Data are returned but the ID is %d", suppliers[0].SupplierID)
	}
}

func TestGetHeavyUsersAll(t *testing.T) {
	heavyUsers := GetHeavyUsersAll()
	if len(heavyUsers) <= 1 {
		t.Error("Only zero or one heavy user entry is returned. Want more than one.")
	}
}
