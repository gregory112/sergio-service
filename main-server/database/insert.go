package database

import (
	"sergio-service/main-server/utility"
	"sergio-service/models"
)

// UpdateSupplier updates the supplier data based on the data formatted in SensorData struct.
func UpdateSupplier(data utility.SensorData) (err error) {
	supplier := models.Supplier{}

	Mu.Lock()
	defer Mu.Unlock()
	err = DB.Where("supplier_id = ?", data.Id).Find(&supplier).Error
	if err != nil {
		return
	}

	supplier.Volume = data.Volume
	supplier.Flow = data.Flow
	supplier.Active = true
	supplier.LastUpdate = data.Time

	return DB.Model(&supplier).Where("supplier_id = ?", data.Id).Update(&supplier).Error
}

// UpdateAnalytic inserts new analytical history data to analytic table in database based on data sent by the sensor.
func UpdateAnalytic(data utility.SensorData) (err error) {
	analytic := models.Analytic{
		SupplierID: data.Id,
		Volume:     data.Volume,
		Flow:       data.Flow,
		UpdateTime: data.Time,
	}

	Mu.Lock()
	defer Mu.Unlock()
	return DB.Save(&analytic).Error
}

// AddHeavyUser adds new heavy user.
func AddHeavyUser(data *models.HeavyUser) (err error) {
	Mu.Lock()
	defer Mu.Unlock()
	return DB.Save(&data).Error
}

// StopSupplier sets the supplier to inactive.
// StopSupplier basically updates the supplier with certain ID setting the active to false, volume and flow to zero.
// Doing this allows the the interface or other clients to know if the sensor is still active.
func StopSupplier(id int) (err error) {
	Mu.Lock()
	defer Mu.Unlock()

	supplier := models.Supplier{}
	err = DB.Where("supplier_id = ?", id).Find(&supplier).Error
	if err != nil {
		return
	}

	return DB.Model(&supplier).Where("supplier_id = ?", id).Updates(map[string]interface{}{"volume": 0, "flow": 0, "active": false}).Error
}
