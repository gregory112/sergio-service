// Package database provides helper functions for the main-server.
// This package provides few functions for getting data and inserting some data to the database. This package has an
// init function for opening a database connection.
package database

import (
	"log"
	"sync"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var Mu sync.Mutex // Guards DB
var DB *gorm.DB

// init function for the database package initializes the DB package variable with mysql driver.
func init() {
	var err error
	DB, err = gorm.Open("mysql", "root:@/supply_database")

	if err != nil {
		log.Println(err)
		return
	}
	//DB.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&models.Supplier{})
	//DB.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&models.Analytic{})
}
