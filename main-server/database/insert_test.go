package database

import (
	"testing"

	"sergio-service/main-server/utility"
	"sergio-service/models"
)

func TestSyncSupplier(t *testing.T) {
	data := utility.SensorData{
		Id:     1,
		Volume: 1.2,
		Flow:   1.2,
		Time:   102102012,
	}
	UpdateSupplier(data)

	supplier := models.Supplier{}
	DB.First(&supplier)
	if supplier.SupplierID != 1 {
		t.Errorf("SupplierID mismatch, got %d, want %d", supplier.SupplierID, data.Id)
	}
}

func TestUpdateAnalytic(t *testing.T) {
	data := utility.SensorData{
		Id:     1,
		Volume: 1.31,
		Flow:   1.2,
		Time:   102102012,
	}

	UpdateSupplier(data)
	err := UpdateAnalytic(data)
	if err != nil {
		t.Fatal(err)
	}

	analytic := models.Analytic{}
	DB.Order("analytic_id ASC").Find(&analytic)
	if analytic.UpdateTime != 102102012 {
		t.Errorf("UpdateTime mismatch, got %d, want %d", analytic.UpdateTime, data.Time)
	}
}

func TestAddHeavyUser(t *testing.T) {
	data := models.HeavyUser{
		Name:       "PT. Aji Logam",
		MaxFlow:    10.2333,
		SupplierID: 1,
	}

	AddHeavyUser(&data)

	heavyUser := models.HeavyUser{}
	DB.Where("supplier_id = ?", data.SupplierID).Order("user_id ASC").Find(&heavyUser)
	if heavyUser.Name != "PT. Aji Logam" {
		t.Errorf("Name mismatch, got %s, want %s", heavyUser.Name, data.Name)
	}
}

func TestStopSupplier(t *testing.T) {
	data := utility.SensorData{
		Id:     1,
		Volume: 1.2,
		Flow:   1.2,
		Time:   102102012,
	}
	UpdateSupplier(data)
	StopSupplier(data.Id)

	supplier := models.Supplier{}
	DB.Where("supplier_id = ?", data.Id).Find(&supplier)
	if supplier.Active != false {
		t.Error("Supplier is still active after StopSupplier has been called.")
	}
}
