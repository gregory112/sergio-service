package database

import (
	"log"

	"sergio-service/models"
)

const BaseRadius = float32(0.5)

// GetSupplier fetches supplier with specific ID from database
func GetSupplier(id int) (supplier *models.Supplier) {
	supplier = &models.Supplier{}
	err := DB.Where("supplier_id = ?", id).Find(supplier).Error
	if err != nil {
		log.Printf("Error while getting supplier data: %s", err)
		supplier = nil
	}
	return
}

// GetSupplierAll fetches all supplier data without analytic or heavy users data.
func GetSupplierAll() (suppliers []models.Supplier) {
	suppliers = []models.Supplier{}
	err := DB.Find(&suppliers).Error
	if err != nil {
		log.Printf("Error while getting all supplier data: %s", err)
		suppliers = nil
	}
	return
}

// GetSupplierAllWithAnalytics fetches all suppliers data with analytic data.
func GetSupplierAllWithAnalytics() (suppliers []*models.Supplier) {
	suppliers = []*models.Supplier{}
	err := DB.Find(&suppliers).Error
	if err != nil {
		log.Printf("Error while getting all supplier data: %s", err)
		suppliers = nil
	}
	for _, supplier := range suppliers {
		supplier.Analytics = GetAnalytics(supplier.SupplierID)
	}
	return
}

// GetSupplierAllWithHeavyUsers fetches all suppliers data with heavy users data.
func GetSupplierAllWithHeavyUsers() (suppliers []*models.Supplier) {
	suppliers = []*models.Supplier{}
	err := DB.Find(&suppliers).Error
	if err != nil {
		log.Printf("Error while getting all supplier data: %s", err)
		suppliers = nil
	}
	for _, supplier := range suppliers {
		supplier.HeavyUsers = GetHeavyUsers(supplier.SupplierID)
	}
	return
}

// GetSupplierWithAll fetches supplier data with specific ID from database along with Analytics and Heavy Users data.
func GetSupplierWithAll(id int) (supplier *models.Supplier) {
	supplier = &models.Supplier{
		SupplierID: id,
		Analytics:  GetAnalytics(id),
		HeavyUsers: GetHeavyUsers(id),
	}
	err := DB.Where("supplier_id = ?", id).Find(supplier).Error
	if err != nil {
		log.Printf("Error while getting supplier data (with all data): %s", err)
		supplier = nil
	}
	return
}

// GetSupplierWithAnalytics gets supplier data along with all the analytics data.
func GetSupplierWithAnalytics(id int) (supplier *models.Supplier) {
	supplier = &models.Supplier{
		SupplierID: id,
		Analytics:  GetAnalytics(id),
	}
	err := DB.Where("supplier_id = ?", id).Find(supplier).Error
	if err != nil {
		log.Printf("Error while getting supplier data (with analytics): %s", err)
		supplier = nil
	}

	// DOES NOT WORK DON'T KNOW WHY PROBABLY A BUG FROM GORM
	//err := DB.Preload("Supplier.Analytics").Find(supplier).Error
	//err := DB.Model(supplier).Related(&supplier.Analytics).Error
	//log.Print(err)

	return
}

// GetAnalytics gets analytic data based on specific supplier ID.
// GetAnalytics is actually a helper function for GetSupplierWithAnalytics as Preload and Related methods from GORM
// does not work, so analytic data must be fetched manually with Find and Where. GetAnalytics also fetches only the
// latest 5 analytic data sorted by newest.
func GetAnalytics(id int) (analytics []*models.Analytic) {
	analytics = []*models.Analytic{}
	err := DB.Order("analytic_id DESC").Where("supplier_id = ?", id).Limit(5).Find(&analytics).Error
	if err != nil {
		log.Printf("Error while getting analytics data: %s", err)
		analytics = nil
	}
	return
}

// GetSupplierWithHeavyUsers fetches supplier data along with all the supplier's heavy users.
// Heavy users data are fetched from heavy users table indicating registered users with heavy water usage.
func GetSupplierWithHeavyUsers(id int) (supplier *models.Supplier) {
	supplier = &models.Supplier{
		SupplierID: id,
		HeavyUsers: GetHeavyUsers(id),
	}
	err := DB.Where("supplier_id = ?", id).Find(supplier).Error
	if err != nil {
		log.Printf("Error while getting supplier data (with heavy users): %s", err)
		supplier = nil
	}
	return
}

// GetHeavyUsers fetches heavy users data based on SupplierID.
// This function is actually a helper function for GetSupplierWithHeavyUsers but can be called manually.
func GetHeavyUsers(id int) (heavyUsers []*models.HeavyUser) {
	heavyUsers = []*models.HeavyUser{}
	err := DB.Where("supplier_id = ?", id).Find(&heavyUsers).Error
	if err != nil {
		log.Printf("Error while getting heavy users data: %s", err)
		heavyUsers = nil
	}
	return
}

// GetHeavyUsersAll fetches all heavy users data.
func GetHeavyUsersAll() (heavyUsers []*models.HeavyUser) {
	heavyUsers = []*models.HeavyUser{}
	err := DB.Find(&heavyUsers).Error
	if err != nil {
		log.Printf("Error while getting heavy users data: %s", err)
		heavyUsers = nil
	}
	return
}

// GetClosestSupplier gets closest suppliers from given location.
// GetClosestSupplier will try to find suppliers in radius of 10, and if no suppliers found it will repeat the search
// with radius of 10 + 10, then 10 + 20, then 10 + 40. If no supplier is found then nil is returned.
func GetClosestSupplier(lat float32, lng float32, maxFlow float32) (suppliers []models.Supplier) {
	suppliers = []models.Supplier{}
	radius := BaseRadius

	for i := 1; i <= 3; i++ {

		addedLat := lat + radius
		addedLng := lng + radius
		subLat := lat - radius
		subLng := lng - radius

		err := DB.Where("`lat` <= ? AND `long` <= ? AND `lat` >= ? AND `long` >= ? AND `max_flow` >= ?",
			addedLat, addedLng, subLat, subLng, maxFlow).Find(&suppliers).Error
		if err != nil {
			log.Printf("Error while getting closest supplier data: %s", err)
			suppliers = nil
			return
		}

		if len(suppliers) > 0 {
			break
		}

		radius += float32(float32(0.2) * float32(i))
	}

	if len(suppliers) == 0 {
		//log.Printf("Closest suppliers queried but not found. %v", suppliers)
		return nil
	}

	return
}
