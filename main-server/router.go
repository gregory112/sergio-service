package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"sergio-service/main-server/database"
	"sergio-service/main-server/receiver"

	"github.com/gorilla/mux"
)

var router *mux.Router
var server *http.Server

func init() {
	router = mux.NewRouter()

	server = &http.Server{
		Addr:         "0.0.0.0:8080",
		Handler:      router,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
}

// SetRoute attaches all required handler to the http server including supplier data receiver, web server and more.
func SetRoute() {
	receiver.AttachAddData(router)
	receiver.AttachTimeout()

	// URL: /heavy
	AttachUsersAll(router)

	// URL: /supplier/...
	AttachUsers(router)
	AttachAnalytic(router)
	AttachSupplierExtra(router)
	AttachSupplier(router)

	// URL: /supplier
	AttachSupplierAllAnalytics(router)
	AttachSupplierAllUsers(router)
	AttachSupplierAllClosest(router)
	AttachSupplierAll(router)

	// URL: /
	router.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("static"))))
}

// Listen is a wrapper to ListenAndServe.
// This method is created in the first place to make SetRoute independent and testable, but also encapsulates the server
// object so users do not have to call server.ListenAndServe()
func Listen() {
	log.Println("Server started in 8080.")
	log.Fatal(server.ListenAndServe())
}

// AttachSupplier function attaches handler to /supplier/{id} but without querystring.
// This handler should be called last so that other handlers can catch the url. In other words, there are other handlers
// that handle requests to /supplier/{id}?all or /supplier/{id}?users, but this handler is a wild card handler that should
// be called last as a last resort handler when no other handlers matched. Even if it's a wild card handler, it handles
// only /supplier/{id}. Use AttachSupplierAll to handle /supplier.
func AttachSupplier(router *mux.Router) {
	router.HandleFunc("/supplier/{id}", func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		id, err := strconv.Atoi(vars["id"])
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}

		writer.Header().Add("Content-Type", "application/json")
		encoder := json.NewEncoder(writer)
		err = encoder.Encode(database.GetSupplier(id))

		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}

	})
}

// AttachSupplierExtra attaches a new function to accepts supplier queries along with analytic and users data if necesseary.
func AttachSupplierExtra(router *mux.Router) {
	router.HandleFunc("/supplier/{id}", func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		id, err := strconv.Atoi(vars["id"])
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}

		writer.Header().Add("Content-Type", "application/json")
		encoder := json.NewEncoder(writer)
		err = encoder.Encode(database.GetSupplierWithAll(id))
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}

	}).Queries("all", "{all}")
}

// AttachSupplierAllAnalytics attaches a new function to get all suppliers with all analytic data.
func AttachSupplierAllAnalytics(router *mux.Router) {
	router.HandleFunc("/supplier", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Add("Content-Type", "application/json")
		encoder := json.NewEncoder(writer)
		err := encoder.Encode(database.GetSupplierAllWithAnalytics())
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}
	}).Queries("analytic", "")
}

// AttachSupplierAllUsers attaches a new function to get all suppliers with all heavy users data.
func AttachSupplierAllUsers(router *mux.Router) {
	router.HandleFunc("/supplier", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Add("Content-Type", "application/json")
		encoder := json.NewEncoder(writer)
		err := encoder.Encode(database.GetSupplierAllWithHeavyUsers())
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}
	}).Queries("users", "")
}

// AttachSupplierAllClosest attaches a new function to find the closest suppliers
func AttachSupplierAllClosest(router *mux.Router) {
	router.HandleFunc("/supplier", func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		lat, err := strconv.ParseFloat(vars["lat"], 32)
		if err != nil {
			log.Println(err)
			writer.WriteHeader(404)
			return
		}
		lng, err := strconv.ParseFloat(vars["lng"], 32)
		if err != nil {
			log.Println(err)
			writer.WriteHeader(404)
			return
		}
		flow, err := strconv.ParseFloat(vars["flow"], 32)
		if err != nil {
			log.Println(err)
			writer.WriteHeader(404)
			return
		}

		writer.Header().Add("Content-Type", "application/json")
		encoder := json.NewEncoder(writer)
		err = encoder.Encode(database.GetClosestSupplier(float32(lat), float32(lng), float32(flow)))
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}
	}).Queries("lat", "{lat}").Queries("lng", "{lng}").Queries("flow", "{flow}")
}

// AttachSupplierAll adds new handler to /supplier to fetch all supplier data (without analytic and heavy users data).
// This function should be called after AttachSupplierExtra (that handles /user/{id}?querystring and AttachSupplier (that
// handles /user/{id}.
func AttachSupplierAll(router *mux.Router) {
	router.HandleFunc("/supplier", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Add("Content-Type", "application/json")
		encoder := json.NewEncoder(writer)
		err := encoder.Encode(database.GetSupplierAll())
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}
	})
}

// AttachUsers attaches a new function to accepts supplier queries along with heavy users data.
func AttachUsers(router *mux.Router) {
	router.HandleFunc("/supplier/{id}", func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		id, err := strconv.Atoi(vars["id"])
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}

		writer.Header().Add("Content-Type", "application/json")
		encoder := json.NewEncoder(writer)
		err = encoder.Encode(database.GetSupplierWithHeavyUsers(id))

		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}

	}).Queries("users", "{users}")
}

// AttachUsersAll registers a handler to /heavy to fetch all heavy users data (without supplier data).
func AttachUsersAll(router *mux.Router) {
	router.HandleFunc("/heavy", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Add("Content-Type", "application/json")
		encoder := json.NewEncoder(writer)
		err := encoder.Encode(database.GetHeavyUsersAll())

		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}

	})
}

// AttachAnalytic attaches a new function to accepts supplier queries along with analytic data.
func AttachAnalytic(router *mux.Router) {
	router.HandleFunc("/supplier/{id}", func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		id, err := strconv.Atoi(vars["id"])
		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}

		writer.Header().Add("Content-Type", "application/json")
		encoder := json.NewEncoder(writer)
		err = encoder.Encode(database.GetSupplierWithAnalytics(id))

		if err != nil {
			log.Println(err)
			writer.WriteHeader(500)
			return
		}

	}).Queries("analytic", "")
}
