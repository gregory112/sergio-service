package main

import (
	"testing"
	"time"

	"sergio-service/models"
)

func TestWipe(t *testing.T) {
	analytic := &models.Analytic{
		SupplierID: 1,
		UpdateTime: time.Now().Unix() - 8*24*60*60,
	}
	analytic2 := &models.Analytic{
		SupplierID: 1,
		UpdateTime: 0,
	}
	DB.Save(analytic)
	DB.Save(analytic2)

	Wipe()
	analytic = &models.Analytic{
		SupplierID: 888, // Non existent data
	}
	limit := time.Now().Unix() - 7*24*60*60
	DB.Where("`update_time` < ?", limit).Find(analytic)

	if analytic.SupplierID != 888 {
		t.Errorf("Data are not deleted.")
	}

	cleanerLog := &models.CleanerLog{}
	DB.Order("status_id DESC").Last(cleanerLog)
	t.Logf("Newest delete status ID: %d", cleanerLog.StatusID)

}
