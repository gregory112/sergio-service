package main

import (
	"log"
	"time"

	"sergio-service/models"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var DB *gorm.DB

// init function for the database package initializes the DB package variable with mysql driver.
func init() {
	var err error
	DB, err = gorm.Open("mysql", "root:@/supply_database")

	if err != nil {
		log.Fatal(err)
	}
	//DB.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&models.Supplier{})
	//DB.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&models.Analytic{})
}

// Wipe will delete any analytic record older than a week from the time when Wipe is called.
// When Wipe detects no row deleted, Wipe will not call update so the database is not updated. Wipe will only call
// update whenever it finds any row to be deleted.
func Wipe() (err error) {
	limit := time.Now().Unix() - 7*24*60*60
	query := DB.Where("`update_time` <= ?", limit).Delete(models.Analytic{})
	err = query.Error
	if err != nil {
		log.Printf("Deletion is unsuccessful: %s", err)
		return
	}

	if query.RowsAffected > 0 {
		err = update(query.RowsAffected)
		if err != nil {
			log.Printf("WARNING: Deletion seems to be successful but status update failed: %s", err)
			return
		}
		log.Printf("Successfully deleted %d rows.", query.RowsAffected)
	}
	return
}

// update is a helper function that adds the deletion status data to database.
// This function should be called every time the Wipe function is called.
func update(count int64) (err error) {
	cleanerLog := &models.CleanerLog{
		RowCount: count,
		LastRun:  time.Now().Unix(),
	}
	err = DB.Save(cleanerLog).Error
	return
}
