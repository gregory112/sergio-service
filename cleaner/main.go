package main

import "time"

func main() {
	timer := time.NewTicker(60 * time.Second)
	for {
		select {
		case <-timer.C:
			Wipe()
		}
	}
}
