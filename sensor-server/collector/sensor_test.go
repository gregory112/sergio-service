package collector

import (
	"testing"
	"time"
)

func TestSensor_Start(t *testing.T) {
	tests := []struct {
		dry        bool
		level      int
		wantVolume float32
		wantFlow   float32
		wantTime   time.Time
	}{
		{dry: false, level: 1, wantVolume: 15332.221, wantFlow: 153.221, wantTime: time.Now()},
		{dry: false, level: 2, wantVolume: 20123.856, wantFlow: 211.856, wantTime: time.Now()},
		{dry: false, level: 3, wantVolume: 24233.123, wantFlow: 253.123, wantTime: time.Now()},
		{dry: false, level: 4, wantVolume: 30213.132, wantFlow: 303.132, wantTime: time.Now()},
		{dry: true, level: 1, wantVolume: 15332.221 * 0.8, wantFlow: 153.221 * 0.7, wantTime: time.Now()},
		{dry: true, level: 2, wantVolume: 20123.856 * 0.8, wantFlow: 211.856 * 0.7, wantTime: time.Now()},
		{dry: true, level: 3, wantVolume: 24233.123 * 0.8, wantFlow: 253.123 * 0.7, wantTime: time.Now()},
		{dry: true, level: 4, wantVolume: 30213.132 * 0.8, wantFlow: 303.132 * 0.7, wantTime: time.Now()},
		{dry: false, level: 0, wantVolume: 0, wantFlow: 0},
		{dry: true, level: 0, wantVolume: 0, wantFlow: 0},
	}
	sensor := NewSensor(1)
	for _, test := range tests {

		sensor.Start(test.dry, test.level)

		sensor.Mu.Lock()
		volume, _ := sensor.GetVolume()
		flow, _ := sensor.GetFlow()
		sensorTime, _ := sensor.GetTime()
		sensor.Mu.Unlock()

		sensor.Stop()

		if volume > test.wantVolume {
			t.Errorf("Want under %.4f, got %.4f", test.wantVolume, volume)
		}
		if flow > test.wantFlow {
			t.Errorf("want under %.4f, got %.4f", test.wantFlow, flow)
		}
		if (sensorTime == time.Time{}) != (test.wantTime == time.Time{}) {
			t.Errorf("want under %s, got %s", test.wantTime, sensorTime)
		}

		//sensor.completeWait.Wait()
	}
}

func TestSensor_Stop(t *testing.T) {
	sensor := NewSensor(1)
	sensor.Start(false, 1)
	sensor.Stop()

	_, errVol := sensor.GetVolume()
	_, errFlow := sensor.GetFlow()
	_, errTime := sensor.GetTime()

	if errVol == nil {
		t.Error("Can still retrieve Volume after sensor is killed")
	}
	if errFlow == nil {
		t.Error("Can still retrieve Flow after sensor is killed")
	}
	if errTime == nil {
		t.Error("Can still retrieve Time after sensor is killed")
	}
}
