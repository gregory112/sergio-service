// Package collector provides function to collect (dummy data) from a dummy "sensor".
// For now it just randomizes data periodically.
package collector

import (
	"errors"
	"log"
	"sync"
	"time"
)

// Sensor is a type representing a "sensor".
type Sensor struct {
	Id           uint8      `json:"id"`
	Volume       float32    `json:"volume"`
	Flow         float32    `json:"flow"`
	active       bool       `json:"-"`
	Time         time.Time  `json:"time"`
	Mu           sync.Mutex `json:"-"`
	VolumeRandom Randomizer `json:"-"`
	FlowRandom   Randomizer `json:"-"`
	Timer        Timer      `json:"-"`
}

// Randomizer is a function for randomizing sensor data.
// collector package provides the default randomizer function that is used by default. Custom Randomizer can be supplied
// to the Sensor struct when necessary for example testing.
type Randomizer func(dry bool, level int) (value float32, err error)

// Timer is a function to get current time. Should use this for testing.
type Timer func() (value time.Time)

var initWait, completeWait sync.WaitGroup

// NewSensor returns an address to a new sensor with specified Id
func NewSensor(id int) (sensor *Sensor) {
	sensor = &Sensor{
		Id:           uint8(id),
		VolumeRandom: randomizeVolume,
		FlowRandom:   randomizeFlow,
		Timer: func() (value time.Time) {
			return time.Now()
		},
	}
	return
}

// GetId returns the sensor's ID.
// Sensor ID was set initially during creation with NewSensor
func (sensor *Sensor) GetId() int {
	return int(sensor.Id)
}

// GetVolume returns the sensor current Volume at a Time
// It returns an error if sensor is not active.
func (sensor *Sensor) GetVolume() (volume float32, err error) {
	if !sensor.active {
		err = errors.New("collector: cannot get Volume from inactive sensor")
		return
	}
	volume = sensor.Volume
	return
}

// GetFlow returns the sensor current Flow at a Time
// It returns an error if sensor is not active.
func (sensor *Sensor) GetFlow() (flow float32, err error) {
	if !sensor.active {
		err = errors.New("collector: cannot get Flow from inactive sensor")
		return
	}
	flow = sensor.Flow
	return
}

// GetActive returns the sensor current status
func (sensor *Sensor) GetActive() bool {
	return sensor.active
}

// GetTime returns the sensor last collection Time in Time.Time
func (sensor *Sensor) GetTime() (time time.Time, err error) {
	if !sensor.active {
		err = errors.New("collector: cannot get last collection Time from inactive sensor")
		return
	}
	time = sensor.Time
	return
}

// fetchCurrentVolume uses the assistance of randomizeVolume utility function to generate dummy Volume values
// It simulates the Volume data collection as in the sensor was deployed and connected to a real physical sensor
func (sensor *Sensor) fetchCurrentVolume(dry bool, level int) (err error) {
	if !sensor.active {
		err = errors.New("collector: cannot collect Volume data from inactive sensor")
		return
	}
	sensor.Volume, err = sensor.VolumeRandom(dry, level)
	return
}

// fetchCurrentFlow uses the assistance of randomizeFlow utility function to generate dummy Flow values
// It simulates the Flow data collection as in the sensor was deployed and connected to a real physical sensor
func (sensor *Sensor) fetchCurrentFlow(dry bool, level int) (err error) {
	if !sensor.active {
		err = errors.New("collector: cannot collect Flow data from inactive sensor")
		return
	}
	sensor.Flow, err = sensor.FlowRandom(dry, level)
	return
}

// Start kicks in a goroutine to update Volume and Flow data every 30 seconds
// It uses a mutex to lock Volume and Flow variable so that they can be updated simultaneously. Start also uses a
// WaitGroup to wait until the Volume and Flow are initialized for the first Time. This is useful so that other methods
// do not get zero values
func (sensor *Sensor) Start(dry bool, level int) {
	if sensor.active {
		log.Printf("Supplier sensor %d is already started", sensor.GetId())
		return
	}
	sensor.Volume = 0
	sensor.Flow = 0
	sensor.active = true
	completeWait.Add(1)
	initWait.Add(1)
	go func() {
		defer completeWait.Done()
		var err error
		log.Printf("Supplier sensor %d started at %s", sensor.GetId(), time.Now().Format(time.UnixDate))

		initialized := false

		for {
			sensor.Mu.Lock()
			if !sensor.active {
				sensor.Mu.Unlock()
				break
			}
			sensor.Time = sensor.Timer()
			err = sensor.fetchCurrentVolume(dry, level)
			//Volume, _ := sensor.GetVolume()
			//log.Printf("Supplier sensor %d Volume: %.4f", sensor.GetId(), Volume)
			if err != nil {
				log.Print(err)
				sensor.stop()
			} else {
				err = sensor.fetchCurrentFlow(dry, level)
				//Flow, _ := sensor.GetFlow()
				//log.Printf("Supplier sensor %d Flow: %.4f", sensor.GetId(), Flow)
				if err != nil {
					log.Print(err)
					sensor.stop()
				}
			}

			sensor.Mu.Unlock()

			if !initialized {
				initialized = true
				initWait.Done()
			}

			time.Sleep(2 * time.Second)
		}
	}()
	initWait.Wait()
}

// Stop changes the state of the sensor to inactive
// It also resets the Volume and Flow to 0 so that data cannot be received. To start the sensor again, use Start
func (sensor *Sensor) Stop() {
	if !sensor.active {
		log.Printf("Supplier sensor %d is not active", sensor.GetId())
		return
	}
	sensor.Mu.Lock()
	sensor.stop()
	sensor.Mu.Unlock()
	completeWait.Wait()
}

// stop (lowercase) is an alternative Stop method without mutex
func (sensor *Sensor) stop() {
	sensor.Volume = 0
	sensor.Flow = 0
	sensor.active = false
	sensor.Time = time.Time{}
	log.Printf("Supplier sensor %d killed at %s", sensor.GetId(), time.Now().Format(time.UnixDate))
}
