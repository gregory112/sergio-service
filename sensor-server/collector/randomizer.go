package collector

import (
	"errors"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

// randomizeVolume generates random volume based on level and dry status.
// It returns highest value when level is 4 and returns lowest value when level is 1.
// dry bool indicates dry season. Setting this to true cuts the volume output by 20%.
func randomizeVolume(dry bool, level int) (volume float32, err error) {
	var min, max float32
	switch level {
	case 1:
		min = 10234.232
		max = 15332.221
		break
	case 2:
		min = 14232.112
		max = 20123.856
		break
	case 3:
		min = 18233.466
		max = 24233.123
	case 4:
		min = 22333.838
		max = 30213.132
	default:
		volume = 0
		err = errors.New("randomizer: unsupported random volume level")
		return
	}

	randVolume := rand.Float32()*(max-min) + min
	if dry {
		volume = randVolume * 0.8
	} else {
		volume = randVolume
	}
	err = nil
	return
}

// randomizeFlow generates random flow based on level and dry status.
// It returns highest value when level is 4 and returns lowest value when level is 1.
// dry bool indicates dry season. Setting this to true cuts the flow output by 30% indicating the lack of water
// supplies.
func randomizeFlow(dry bool, level int) (flow float32, err error) {
	var min, max float32
	switch level {
	case 1:
		min = 123.232
		max = 153.221
		break
	case 2:
		min = 142.112
		max = 211.856
		break
	case 3:
		min = 201.466
		max = 253.123
	case 4:
		min = 223.838
		max = 303.132
	default:
		flow = 0
		err = errors.New("randomizer: unsupported random flow level")
		return
	}

	randFlow := rand.Float32()*(max-min) + min
	if dry {
		flow = randFlow * 0.7
	} else {
		flow = randFlow
	}
	err = nil
	return
}
