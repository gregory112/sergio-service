// The main package contains files that will do data collection and send it to the server
package main

import (
	"flag"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"sergio-service/sensor-server/collector"
)

var id = flag.Int("id", rand.Intn(5)+1, "Defines ID for this sensor")
var url = flag.String("url", "localhost", "Set the receiver host (example 123.123.123.123). Default is localhost")

func main() {
	flag.Parse()
	ticker := time.NewTicker(30 * time.Second)
	quit := make(chan struct{})

	sensor := collector.NewSensor(*id)
	sensor.Start(false, rand.Intn(3)+1)
	defer sensor.Stop()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for sig := range c {
			if sig == syscall.SIGINT {
				log.Print("SIGINT is detected, stopping sensor")
				close(quit)
				break
			}
		}
	}()

	fail := 0

	for {
		select {
		case <-ticker.C:
			jsonString, err := ToJson(sensor)

			if err != nil {
				close(quit)
			}

			err = SendData("http://"+*url+"/upload/"+strconv.Itoa(*id), jsonString)
			if err != nil {
				log.Print(err)
				fail++
				if fail == 5 {
					log.Print("Sensor is stopped after too many failed connections.")
					close(quit)
				}
			} else {
				fail = 0
			}
		case <-quit:
			ticker.Stop()
			return
		}
	}
}
