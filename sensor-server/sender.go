package main

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"sergio-service/sensor-server/collector"
)

// ToJson is a function for converting sensor data into JSON string
// It uses collect utility function to atomically collect id, volume, flow, and update time from a given sensor, then
// convert it into JSON string. Time is converted into seconds since epoch.
func ToJson(sensor *collector.Sensor) (jsonString string, err error) {

	_, volume, flow, sensorTime, err := collect(sensor)
	if err != nil {
		log.Printf("toJson: %s", err)
		return
	}
	timeEpoch := sensorTime.Unix()

	jsonString = fmt.Sprintf("{\"volume\":%.4f,\"flow\":%.4f,\"time\":%d}", volume, flow, timeEpoch)
	return
}

// SendData is a function for making POST request to server along with the sensor data formatted in JSON
// It accepts address as string and JSON data as string. SendData returns error if the HTTP response code is not 200.
// This function also omits the response body.
func SendData(address string, json string) (err error) {
	resp, err := http.Post(address, "application/json", bytes.NewBufferString(json))
	if err != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		err = errors.New(fmt.Sprintf("sendData: response is %d", resp.StatusCode))
	} else {
		log.Printf("Sending data to %s successful.", address)
	}

	return
}

// collect is a helper function to collect id, volume, flow, and update time from a sensor atomically
func collect(sensor *collector.Sensor) (id int, volume float32, flow float32, sensorTime time.Time, err error) {
	sensor.Mu.Lock()
	defer sensor.Mu.Unlock()
	id = sensor.GetId()
	volume, err = sensor.GetVolume()
	if err != nil {
		return
	}
	flow, err = sensor.GetFlow()
	if err != nil {
		return
	}

	sensorTime, err = sensor.GetTime()

	return
}
