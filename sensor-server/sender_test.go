package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"sergio-service/sensor-server/collector"
)

func TestToJson(t *testing.T) {
	tests := []struct {
		dry   bool
		level int
		want  string
	}{
		{dry: false, level: 2, want: "{\"id\":2,\"volume\":1.0000,\"flow\":1.0000,\"time\":1}"},
		{dry: true, level: 5, want: "{\"id\":2,\"volume\":1.0000,\"flow\":1.0000,\"time\":1}"},
	}

	var fakeRandomize collector.Randomizer = func(dry bool, level int) (value float32, err error) {
		value = 1.0
		return
	}

	var fakeTimer collector.Timer = func() (value time.Time) {
		value = time.Unix(1, 0)
		return
	}

	sensor := collector.NewSensor(2)
	sensor.VolumeRandom = fakeRandomize
	sensor.FlowRandom = fakeRandomize
	sensor.Timer = fakeTimer

	for _, test := range tests {
		sensor.Start(test.dry, test.level)
		result, err := ToJson(sensor)
		if result != test.want || err != nil {
			t.Errorf("got %s, want %s", result, test.want)
		}
		sensor.Stop()
	}
}

func TestSendData(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {

		var content = struct {
			A int    `json:"a"`
			B string `json:"b"`
		}{}

		defer request.Body.Close()
		/*body, err := ioutil.ReadAll(request.Body)
		t.Logf("raw request body: %v", string(body))

		if err != nil {
			t.Fatal(err)
		}*/

		decoder := json.NewDecoder(request.Body)
		err := decoder.Decode(&content)

		if err != nil {
			t.Fatal(err)
		}

		t.Logf("request header detected content-type: %v", request.Header.Get("Content-Type"))
		if content.A != 1 {
			t.Errorf("want 1 but got %d", content.A)
		}
		if content.B != "abcd" {
			t.Errorf("want abcd but got %s", content.B)
		}
	}))

	defer server.Close()

	err := SendData(server.URL, "{\"a\":1,\"b\":\"abcd\"}")
	if err != nil {
		t.Fatal(err)
	}
}
