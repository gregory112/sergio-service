package models

// Supplier is a model for the supplier data.
// This model is mainly used by all packages and is the primary model used for converting raw supplier data.
type Supplier struct {
	SupplierID   int          `gorm:"primary_key,AUTO_INCREMENT" json:"supplier_id"`
	SupplierName string       `json:"supplier_name"`
	Lat          float32      `json:"lat"`
	Long         float32      `json:"long"`
	Volume       float32      `json:"volume"`
	Flow         float32      `json:"flow"`
	MaxVolume    float32      `json:"max_volume"`
	MaxFlow      float32      `json:"max_flow"`
	Active       bool         `json:"active"`
	LastUpdate   int64        `json:"last_update"`
	Analytics    []*Analytic  `gorm:"ForeignKey:SupplierID" json:"analytics"`
	HeavyUsers   []*HeavyUser `gorm:"ForeignKey:SupplierID" json:"heavy_users"`
}
