package models

// CleanerLog is a model for the cleaner log entries in database.
// This model is used mainly by the cleaner command.
type CleanerLog struct {
	StatusID int   `json:"status_id"`
	RowCount int64 `json:"row_count"`
	LastRun  int64 `json:"last_run"`
}
