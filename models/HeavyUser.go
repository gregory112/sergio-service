package models

// HeavyUser is a model for the heavy users data in database.
type HeavyUser struct {
	UserID     int     `gorm:"primary_key,AUTO_INCREMENT" json:"user_id"`
	Name       string  `json:"name"`
	MaxFlow    float32 `json:"max_flow"`
	SupplierID int     `json:"supplier_id"`
	Warning    bool    `json:"warning"`
}
