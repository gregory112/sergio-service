// models package provides models for the database.
// Models available in this package including Analytic, CleanerLog, HeavyUser, and Supplier. These models are then used
// by ORM and JSON decoder/encoder to transform raw data into structured data or vice versa.
package models

// Analytic is a model for the analytic table.
type Analytic struct {
	AnalyticID int     `gorm:"primary_key,AUTO_INCREMENT" json:"analytic_id"`
	SupplierID int     `json:"supplier_id"`
	Volume     float32 `json:"volume"`
	Flow       float32 `json:"flow"`
	UpdateTime int64   `json:"update_time"`
}
