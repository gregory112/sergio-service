.PHONY: all build install

all:
	@go build -v ./main-server
	@go build -v ./sensor-server
	@go build -v ./cleaner

build:
	@go build -v ./main-server
	@go build -v ./sensor-server
	@go build -v ./cleaner

install:
	@go install -v ./...